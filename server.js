var express = require('express');
var { createLogger, format, transports } = require('winston');
var { combine, timestamp, label, printf } = format;
var bodyParser = require('body-parser');
var jackrabbit = require('jackrabbit');
var rabbitUrl = "amqp://pconpuub:Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax@lion.rmq.cloudamqp.com/pconpuub";
var rabbit = jackrabbit(rabbitUrl);
var exchange = rabbit.default();
var request = require('request');

var app  = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));  

const myFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
  });

  const logger = createLogger({
    format: combine(
      timestamp(),
      myFormat
    ),
    transports: [
        new transports.Console(),
        new transports.File({ filename: 'aguyje.log' })]
  });

var newKudos = exchange.queue({ name: 'newku2', durable: false });
newKudos.consume(onNewKudos);
function onNewKudos(data,ack) {
    logger.info("[Creacion de kudos] Nuevo kudos para el usuario " + data.toString());
    ack();
    exchange.publish({ msg: data.toString() }, { key: 'updateKu2Counter' });
}

var deletedKudos = exchange.queue({ name: 'deletedku2', durable: false });
deletedKudos.consume(onDeletedKudos);
function onDeletedKudos(data,ack) {
    logger.info("[Eliminación de kudos] Kudos eliminado para el usuario " + data.toString());
    ack();
    exchange.publish({ msg: data.toString() }, { key: 'subtractKu2Counter' });
}

var deletedUser = exchange.queue({ name: 'deletedUser', durable: false});
deletedUser.consume(onDeletedUser);
function onDeletedUser(data, ack){
    logger.info("[Eliminacion de usuario] Usuario " + data.msg);
    ack();
    exchange.publish({msg: data.msg}, {key: 'deleteKudos'});
}

app.get('/getUsers/:receiverId/:senderId', function(req, res){
  request('http://localhost:3000/api/users/' + req.params.receiverId + '/' + req.params.senderId, function(error, response, body){
    console.log(response);
    return res.send(body)
  });
});

app.listen(3001);